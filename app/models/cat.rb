class Cat < ActiveRecord::Base
  attr_accessible :age, :birth_date, :color, :name, :sex

  COLORS = %w( white black red green blue )

  validates :age, :birth_date, :color, :name, :sex, :presence => true
  validates :sex, :inclusion => %w( m f )
  validates :color, :inclusion => COLORS


  has_many(
    :rental_requests,
    :class_name => "CatRentalRequest",
    :foreign_key => :cat_id,
    :primary_key => :id,
    :dependent => :destroy
  )

  belongs_to(
    :owner,
    :class_name => "User",
    :foreign_key => :user_id,
    :primary_key => :id
  )

end
