class CatRentalRequest < ActiveRecord::Base
  attr_accessible :cat_id, :start_date, :end_date, :status

  STATUSES = %w(PENDING APPROVED DENIED)

  validates :cat_id, :start_date, :end_date, :status, :presence => true
  validates :status, :inclusion => STATUSES
  validate :no_double_booking


  before_validation :set_status

  belongs_to(
    :cat,
    :class_name => "Cat",
    :foreign_key => :cat_id,
    :primary_key => :id
  )

  def overlapping_requests
    where_cond = <<-SQL
    (cat_id == :cat_id) AND ((end_date BETWEEN :start_date AND :end_date ) OR (start_date BETWEEN :end_date AND :start_date))
    SQL

    CatRentalRequest.where(where_cond, {cat_id: cat_id, start_date: start_date, end_date: end_date} )
  end

  def overlapping_approved_requests
    overlapping_requests.select do |request|
      request.status == "APPROVED" && request.id != self.id
    end
  end

  def overlapping_pending_requests
    overlapping_requests.select do |request|
      request.status == "PENDING" && request.id != self.id
    end
  end

  def no_double_booking
    error.add(:base, "Can't double book a cat") unless overlapping_approved_requests.empty?
  end

  def pending?
    self.status == "PENDING"
  end

  def approve!
    transaction do
      self.status = "APPROVED"
      self.save!
      overlapping_pending_requests.each do |req|
        req.deny!
      end
    end
  end

  def deny!
    self.status = "DENIED"
    self.save!
  end


  def set_status
    self.status ||= "PENDING"
  end


end
