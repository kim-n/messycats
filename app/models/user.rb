require 'bcrypt'

class User < ActiveRecord::Base
  attr_accessible :password, :user_name

  validates :password_digest, :user_name, :session_token, :presence => true

  before_validation :ensure_session_token

  has_many(
    :cats,
    :class_name => "Cat",
    :foreign_key => :user_id,
    :primary_key => :id
  )

  def self.find_by_credentials(user_name, password)
    user = User.find_by_user_name(user_name)
    user if !user.nil? && user.is_password?(password)
  end

  def password=(password)
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end


  def generate_session_token
    SecureRandom.base64(16)
  end

  def reset_session_token!
    self.session_token = generate_session_token
  end

  def ensure_session_token
    self.session_token ||= generate_session_token
  end
end
