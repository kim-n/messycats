class CatsController < ApplicationController
  before_filter :ensure_owner, :only => [:edit, :update]

  def index
    @cats = Cat.all
  end

  def show
    @cat = Cat.find(params[:id])
  end

  def create
    cat = Cat.new(params[:cat])
    cat.user_id = current_user.id

    if cat.save!
      redirect_to cat_url(cat)
    else
      render :text => "ERROR: Cat can't be saved!"
    end
  end

  def edit
    @cat = Cat.find(params[:id])
  end

  def new
    @cat = Cat.new
  end

  def update
    cat = Cat.find(params[:id])

    if cat.update_attributes(params[:cat])
      redirect_to cat_url(cat)
    else
      render :text => "ERROR: Can't update cat!"
    end
  end

  def ensure_owner
    cat = Cat.find(params[:id])
    if !current_user.nil? && current_user.id == cat.owner.id
      return true
    else
      redirect_to new_sessions_url
    end
  end



end
