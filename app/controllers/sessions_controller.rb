class SessionsController < ApplicationController

  def create
    user = User.find_by_credentials(
      params[:user][:user_name],
      params[:user][:password]
    )

    if user.nil?
      render :json => "Credentials were wrong"
    else
      log_in(user)

      redirect_to cats_url
    end
  end

  def destroy
    logout_current_user

    redirect_to new_sessions_url
  end


end
