class UsersController < ApplicationController

  def create
    new_user = User.new(params[:user])

    if new_user.save!
      log_in(new_user)
      redirect_to user_url(new_user)
    else
      render :text => "User NOT Saved"
    end
  end

  def show
    @user = User.find(params[:id])
  end
end
