class CatRentalRequestsController < ApplicationController
  before_filter :ensure_owner, :only => [:accept, :deny]


  def create
    p "PARAMS"
    p params[:rental_requests]
    rental_request = CatRentalRequest.new(params[:rental_requests])

    if rental_request.save!
      redirect_to cat_url(params[:rental_requests][:cat_id])
    else
      render :text => "ERROR: Not a valid request"
    end
  end

  def accept
    req_approve = CatRentalRequest.find(params[:id])
    req_approve.approve!
    redirect_to req_approve.cat

    render :text => params
  end

  def deny
    req_deny = CatRentalRequest.find(params[:id])
    req_deny.deny!
    redirect_to cat_url(req_deny.cat.id)
  end

  def new

  end

  def ensure_owner
    rental = CatRentalRequest.find(params[:id])
    if !current_user.nil? && current_user.id == rental.cat.owner.id
      return true
    else
      redirect_to new_sessions_url
    end
  end

end
