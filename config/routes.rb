NinetyCats::Application.routes.draw do
  resources :cats
  resources :cat_rental_requests do
    member do
      post 'accept'
      post 'deny'
    end
  end
  root to: "cats#index"

  resources :users, :only => [:new, :create, :show]
  resource :sessions
end
